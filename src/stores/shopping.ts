import { defineStore } from "pinia";

export const useShoppingStore = defineStore("shopping", {
  state: () => {
    return {
      shoppingCardData: JSON.parse(localStorage.getItem("data")) ?? [],
    };
  },
});
