// composables/useShop.ts
import { useShoppingStore } from "../stores/shopping";

export const useShop = () => {
  const shopping = useShoppingStore();

  function addShop(shopData: any) {
    localStorage.setItem("data", JSON.stringify(shopData));
    shopping.shoppingCardData = shopData;
  }

  function getShop(keyItem: string) {
    let data = JSON.parse(localStorage.getItem(keyItem) as any) || [];
    shopping.shoppingCardData = data;
    return data;
  }

  function addItem(item: any) {
    let data = getShop("data") ?? [];
    let found = false;

    for (let i = 0; i < data.length; i++) {
      if (data[i].name === item.name && data[i].cafe === item.cafe) {
        data[i].count++;
        found = true;
        break;
      }
    }

    if (!found) {
      data.push({ ...item, count: 1 });
    }

    addShop(data);
    shopping.shoppingCardData = data;
  }

  function removeItem(item: any) {
    let data = getShop("data") ?? [];

    for (let i = 0; i < data.length; i++) {
      if (data[i].name === item.name && data[i].cafe === item.cafe) {
        if (data[i].count > 1) {
          data[i].count--;
        } else {
          data.splice(i, 1);
        }
        break;
      }
    }

    addShop(data);
    shopping.shoppingCardData = data;
  }

  return {
    addShop,
    getShop,
    addItem,
    removeItem,
  };
};
